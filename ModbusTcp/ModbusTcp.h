#pragma once

#ifdef MODBUSTCP_EXPORTS
#define MODBUSTCP_API __declspec(dllexport)
#else
#define MODBUSTCP_API __declspec(dllimport)
#endif


#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <cstdint>
#include <thread>
#include <string>
#include <stdio.h>
#include <cstdio>
#include <iostream>
#include <vector> 
#include <mutex>


class MODBUSTCP_API ModbusTcp
{
public:
	ModbusTcp(boost::asio::io_service &service);
	virtual ~ModbusTcp() { };
	void connect(boost::asio::ip::tcp::endpoint & ep);
	void write(uint8_t(&frame)[12]);
	void read();
	boost::asio::deadline_timer* gett1();
	boost::asio::deadline_timer* gett2();
	std::vector<bool> repCoil;
	std::vector<int> repReg;
private:
	boost::asio::io_service::work work;
	boost::asio::ip::tcp::socket sock;
	uint8_t response[1024];	
	void coilReadHandler();
	void regReadHandler();
	boost::asio::deadline_timer t1;
	boost::asio::deadline_timer t2;
};

class MODBUSTCP_API Coil
{
public:
	Coil(ModbusTcp* modbustcp);
	~Coil() {};
	void readCoil();
	void writeCoil();
	short startAddr, num_val;
private:
	ModbusTcp* m_pModbusTcp;
	uint8_t frame[12];	
	void wait();
	void setFrameFC01();
	void setFrameFC05();
};


class MODBUSTCP_API Register
{
public:
	Register(ModbusTcp* modbustcp);
	~Register() {};
	void readRegister();
	void inputRegister();
	short startAddr, num_val;
private:
	ModbusTcp* m_pModbusTcp;
	
	uint8_t frame[12];
	void wait();
	void setFrameFC03();
	void setFrameFC06();
};