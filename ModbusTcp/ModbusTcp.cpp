// ModbusTcp.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "ModbusTcp.h"
#include <bitset>

ModbusTcp::ModbusTcp(boost::asio::io_service & service) : work(service), sock(service), t1(service), t2(service)
{

}

void ModbusTcp::connect(boost::asio::ip::tcp::endpoint & ep)
{
	sock.async_connect(ep, [this](const boost::system::error_code & ec) {
		if (!ec)
		{
			std::cout << "CONNECTED TO MODBUS SLAVE." << std::endl;
			read();
		}
		else
		{
			std::cout << "No Connection!" << std::endl;
		}
	});
}

void ModbusTcp::write(uint8_t(&frame)[12])
{
	sock.async_write_some(boost::asio::buffer(frame, 12), [this](const boost::system::error_code & e, size_t byte)
	{
		if (!e)
		{
		}
		else
		{
			sock.close();
		}
	});
}

void ModbusTcp::read()
{
	sock.async_read_some(boost::asio::buffer(response, 1024), [this](const boost::system::error_code & e, size_t byte)
	{
		if (!e)
		{
			if (response[1] == 1)
			{
				coilReadHandler();
			}
			if (response[1] == 3)
			{
				regReadHandler();
			}
			if (response[1] == 5)
			{
				std::cout << "-----Wrote to Coil" << std::endl;
			}
			if (response[1] == 6)
			{
				std::cout << "-----Wrote to Register" << std::endl;
			}
			read();
		}
		else
		{
			std::cout << "Read Error!" << std::endl;
		}
	});
}

boost::asio::deadline_timer * ModbusTcp::gett1()
{
	boost::asio::deadline_timer*t = &t1;
	return t;
}

boost::asio::deadline_timer * ModbusTcp::gett2()
{
	boost::asio::deadline_timer*t = &t2;
	return t;
}

void ModbusTcp::coilReadHandler()
{
	std::vector<bool> temp;
	temp = repCoil;
	repCoil.clear();
	std::bitset<6> resultBin(response[9]);
	for (int i = 0; i < 6; i++) // 6 = number of coil
	{
		repCoil.push_back(resultBin[i]);
	}
	if (temp == repCoil)
	{
	}
	if (temp != repCoil)
	{
		std::vector<bool>::iterator i;
		for (i = repCoil.begin(); i != repCoil.end(); i++)
		{
			std::cout << std::boolalpha << *i << " ";
		}
		std::cout << std::endl;
	}
}

void ModbusTcp::regReadHandler()
{
	int size = (response[8]);
	std::vector<int> tempVector;
	tempVector = repReg;
	repReg.clear();
	std::vector<int> rep;

	for (int j = 9; j <= (size + 8); j++)
	{
		rep.insert(rep.end(), (int16_t)(response[j]));

	}
	int count = 0;
	while (count < size)
	{
		int temp = rep[count] * 256 + (rep[count + 1]);
		repReg.insert(repReg.end(), temp);
		count += 2;
	}

	if (tempVector != repReg)
	{
		for (auto k : repReg)
		{
			std::cout << k << " ";
		}
		std::cout << "\n";
	}
	if (tempVector == repReg)
	{

	}
}

Coil::Coil(ModbusTcp*modbustcp) : m_pModbusTcp(modbustcp)
{
	std::cout << "Function code 01" << std::endl;
}

void Coil::readCoil()
{
	setFrameFC01();
	m_pModbusTcp->write(frame);
	wait();
}

void Coil::writeCoil()
{
	setFrameFC05();
	m_pModbusTcp->write(frame);
	wait();
}

void Coil::wait()
{
	m_pModbusTcp->gett1()->expires_from_now(boost::posix_time::seconds(1));
	m_pModbusTcp->gett1()->async_wait([this](const boost::system::error_code & e)
	{
		if (e)
			return;
		readCoil();
	});
}

void Coil::setFrameFC01()
{
	memset(frame, 0, 12);
	startAddr = 0;
	num_val = 6;
	frame[0] = 0;
	frame[1] = 1;
	frame[2] = 0;
	frame[3] = 0;
	frame[4] = 0;
	frame[5] = 6;
	frame[6] = 1;
	frame[7] = 1; ///functioncode
	frame[8] = (uint8_t)(startAddr >> 8);
	frame[9] = (uint8_t)(startAddr);
	frame[10] = (uint8_t)(num_val >> 8);
	frame[11] = (uint8_t)(num_val);
}

void Coil::setFrameFC05()
{
	memset(frame, 0, 12);
	frame[0] = 0;
	frame[1] = 5;
	frame[2] = 0;
	frame[3] = 0;
	frame[4] = 0;
	frame[5] = 6;
	frame[6] = 1;
	frame[7] = 5; ///functioncode
	frame[8] = (uint8_t)(startAddr >> 8);
	frame[9] = (uint8_t)(startAddr);
	frame[10] = (uint8_t)(num_val);
	frame[11] = (uint8_t)(num_val >> 8);
}

Register::Register(ModbusTcp * modbustcp) : m_pModbusTcp(modbustcp)
{
	std::cout << "Function code 03+06" << std::endl;
}

void Register::readRegister()
{
	setFrameFC03();
	m_pModbusTcp->write(frame);
	wait();
}

void Register::inputRegister()
{
	setFrameFC06();
	m_pModbusTcp->write(frame);
	wait();
}

void Register::wait()
{
	m_pModbusTcp->gett2()->expires_from_now(boost::posix_time::seconds(1));
	m_pModbusTcp->gett2()->async_wait([this](const boost::system::error_code & e)
	{
		if (e)
			return;
		readRegister();
	});
}

void Register::setFrameFC03()
{
	memset(frame, 0, 12);
	startAddr = 0;
	num_val = 6;
	frame[0] = 0;
	frame[1] = 3;
	frame[2] = 0;
	frame[3] = 0;
	frame[4] = 0;
	frame[5] = 6;
	frame[6] = 1;
	frame[7] = 3; ///functioncode
	frame[8] = (uint8_t)(startAddr >> 8);
	frame[9] = (uint8_t)(startAddr);
	frame[10] = (uint8_t)(num_val >> 8);
	frame[11] = (uint8_t)(num_val);
}

void Register::setFrameFC06()
{
	memset(frame, 0, 12);
	frame[0] = 0;
	frame[1] = 6;
	frame[2] = 0;
	frame[3] = 0;
	frame[4] = 0;
	frame[5] = 6;
	frame[6] = 1;
	frame[7] = 6; ///functioncode
	frame[8] = (uint8_t)(startAddr >> 8);
	frame[9] = (uint8_t)(startAddr);
	frame[10] = (uint8_t)(num_val >> 8);
	frame[11] = (uint8_t)(num_val);
}
