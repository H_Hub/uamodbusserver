#pragma once
#ifdef READCOILDLL_EXPORTS
#define READCOILDLL_API __declspec(dllexport)
#else 
#define READCOILDLL_API __declspec(dllimport)
#endif

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <cstdint>
#include <thread>
#include <string>
#include <stdio.h>
#include <cstdio>
#include <iostream>
#include <vector>

class READCOILDLL_API ReadCoil
{
public:
	ReadCoil();
	virtual ~ReadCoil();
	void connect(boost::asio::ip::tcp::endpoint& ep);
	void connectHandler(const boost::system::error_code& ec);
	void writeFrame();
	void writeFrameHandler(const boost::system::error_code& ec, size_t byte);
	void readCoil();
	void readCoilHandler(const boost::system::error_code& ec, size_t byte);

	void timeout(const boost::system::error_code&e);

	std::string toBool(char* rep);
	std::vector<std::string> strVector;
	std::vector<std::string> tempVector;

	std::vector<bool> result;


private:
	std::thread threadWork;
	boost::asio::io_service service;
	boost::asio::io_service::work work;
	boost::asio::ip::tcp::socket sock;
	uint8_t response[1024];
	uint8_t frameSender[12];
	short startAddr_, numberOfCoil_;
	boost::asio::deadline_timer t;
	void wait();

};