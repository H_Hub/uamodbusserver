// readcoilDLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "readcoillib.h"
#include <bitset>

void run(boost::asio::io_service* service)
{
	service->run();
}

ReadCoil::ReadCoil() : sock(service), work(service), t(service)
{

	threadWork = std::thread(std::bind(&run, &service));
	memset(frameSender, 0, 12);
	startAddr_ = 0; //start address is set by 0
	numberOfCoil_ = 6; //number of coils to read is set to 6

	frameSender[0] = 0;
	frameSender[1] = 1;
	frameSender[2] = 0;
	frameSender[3] = 0;
	frameSender[4] = 0;
	frameSender[5] = 6;
	frameSender[6] = 1;
	frameSender[7] = 1;
	frameSender[8] = (uint8_t)(startAddr_ >> 8);
	frameSender[9] = (uint8_t)(startAddr_);
	frameSender[10] = (uint8_t)(numberOfCoil_ >> 8);
	frameSender[11] = (uint8_t)(numberOfCoil_);

}

ReadCoil::~ReadCoil()
{
	sock.close();
	service.stop();
	threadWork.join();
}

void ReadCoil::connect(boost::asio::ip::tcp::endpoint & ep)
{
	sock.async_connect(ep, boost::bind(&ReadCoil::connectHandler, this, boost::asio::placeholders::error));
}

void ReadCoil::connectHandler(const boost::system::error_code & ec)
{
	if (!ec)
	{
		printf("\nCONNECTED TO MODBUS SLAVE, FC01\n");
		writeFrame();
	}
	else
	{
		printf("\nNo connection to ModbusSlave\n");
	}

}

void ReadCoil::writeFrame()
{
	sock.async_write_some(boost::asio::buffer(frameSender, 12), boost::bind(&ReadCoil::writeFrameHandler, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void ReadCoil::writeFrameHandler(const boost::system::error_code & ec, size_t byte)
{
	if (!ec)
	{
		readCoil();
	}
	else
	{
		sock.close();
	}

}

void ReadCoil::readCoil()
{
	memset(response, 0, 1024);
	sock.async_read_some(boost::asio::buffer(response, 1024), boost::bind(&ReadCoil::readCoilHandler, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

	wait();
}

void ReadCoil::readCoilHandler(const boost::system::error_code & ec, size_t byte)
{
	if (!ec)
	{
		//tempVector = strVector;
		//strVector.clear();
		//int const size = static_cast <int> (response[8]);
		//char* rep = new char[size];
		//for (int i = 0, j = 9; i < 1, j < size + 9; i++, j++)
		//{
		//	rep[i] = response[j];

		//}
		////std::cout << response[4] << std::endl;
		//std::string data_rep, dt;
		//data_rep = toBool(rep);
		////std::cout << data_rep << std::endl;

		//for (int i = 0; i < data_rep.length(); i += 5)
		//{
		//	dt = data_rep.substr(i, 5);
		//	strVector.push_back(dt);
		//}
		////std::cout << strVector[2] << std::endl;
		//if (tempVector == strVector)
		//{
		//	delete[] rep;
		//	rep = NULL;

		//}
		//if (tempVector != strVector)
		//{
		//	tempVector.clear();
		//	std::cout << data_rep << std::endl;
		//	delete[] rep;
		//	rep = NULL;
		//}
		if (response[1] == 1)
		{

			std::vector<bool>temp;
			temp = result;
			result.clear();
			std::bitset<6> resultBin(response[9]);
			for (int i = 0; i < 5; i++)
			{
				result.push_back(resultBin[i]);
			}
			if (temp == result)
			{
			}
			if (temp != result)
			{
				std::vector<bool>::iterator i;
				for (i = result.begin(); i != result.end(); i++)
				{
					std::cout << std::boolalpha << *i << " ";
				}
				std::cout << std::endl;
			}
		}

	}
	else
	{
		std::cout << "Reading Error!" << std::endl;
	}


}

void ReadCoil::timeout(const boost::system::error_code & e)
{
	if (e)
		return;
	writeFrame();
}

std::string ReadCoil::toBool(char * rep)
{
	std::string statusCoil;
	int const size = static_cast <int> (response[8]);
	std::string data_rep = "";
	for (auto i = 0; i < size; i++)
	{
		int a = rep[i];
		int length_coil = (i == size - 1) ? (numberOfCoil_ % 8) : 8;
		for (int j = 0; j < length_coil; j++)
		{
			int k = a >> j;
			if (k & 1)
			{
				statusCoil = "True ";
			}
			else
			{
				statusCoil = "False";
			}
			data_rep += static_cast<std::string> (statusCoil);
		}
	}

	return data_rep;
}

void ReadCoil::wait()
{
	t.expires_from_now(boost::posix_time::seconds(1)); ///ding nghia cho t la timer wait 1s
	t.async_wait(boost::bind(&ReadCoil::timeout, this, boost::asio::placeholders::error));
}


	



