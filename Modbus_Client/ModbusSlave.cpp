#include "stdafx.h"
#include "ModbusSlave.h"

ModbusSlave::ModbusSlave(boost::asio::io_service & service):mSocket(service), mTimerSecondLoop(service), mEp(boost::asio::ip::address_v4::from_string("127.0.0.1"), 502), mIntervalTime(2)
{

}

unsigned int ModbusSlave::MBRead(RequestInfo & requestInfo)
{
	std::shared_ptr<ModbusADU> modbusADU(new ModbusADU(requestInfo));
	std::shared_ptr<ModbusRequest> modbusRequest(new ModbusRequest(requestInfo, modbusADU));
	if (requestInfo.mFunctionCode != 0)
	{
		modbusRequest->mValid = true;
		mActiveRequests[modbusRequest->mId] = modbusRequest;
	}
	return modbusRequest->mId;
}

bool ModbusSlave::connect()
{
	mSocket.async_connect(mEp, [this](const boost::system::error_code& ec) {
		if (ec.value() != 0) {
			mConnectStatus = false;
			std::cout << ec.message() << std::endl;
		}
		mConnectStatus = true;
	});
	return mConnectStatus;
}

void ModbusSlave::run()
{
	if (mConnectStatus == 1 and mActiveRequests.size() > 0)
	{
		mIt = mActiveRequests.begin();
		writeToSocket();
	}
	mTimerSecondLoop.expires_from_now(boost::posix_time::seconds(mIntervalTime));
	mTimerSecondLoop.async_wait(boost::bind(&ModbusSlave::run, this));
}

void ModbusSlave::MBCancelRequest(unsigned int requestID)
{
	auto cancel_it = mActiveRequests.find(requestID);
	if (cancel_it != mActiveRequests.end())
	{
		mActiveRequests.erase(requestID);
	}
	mIt = mActiveRequests.begin();
}

void ModbusSlave::updateList()
{
	if (mIt->second->m_pModbusADU->getqFcode() == 1)
	{
		for (unsigned int i = 0; i < mIt->second->m_pModbusADU->getqNumberOfData(); i++)
		{
			mCoilList[(i + mIt->second->m_pModbusADU->getqStartAddr())] = mIt->second->m_pModbusADU->mCoilData[i];
			if (mIt->second->m_pModbusADU->getexceptionCode() == 2)
			{
				exceptionRequestCoil.push_back(i + mIt->second->m_pModbusADU->getqStartAddr());
			}
		}
	}
	if (mIt->second->m_pModbusADU->getqFcode() == 2)
	{
		for (unsigned int i = 0; i < mIt->second->m_pModbusADU->getqNumberOfData(); i++)
		{
			mInputCoilList[(i + mIt->second->m_pModbusADU->getqStartAddr())] = mIt->second->m_pModbusADU->mInputCoilData[i];
			if (mIt->second->m_pModbusADU->getexceptionCode() == 2)
			{
				exceptionRequestInputCoil.push_back(i + mIt->second->m_pModbusADU->getqStartAddr());
			}
		}
	}
	if (mIt->second->m_pModbusADU->getqFcode() == 3)
	{
		for (unsigned int i = 0; i < mIt->second->m_pModbusADU->getqNumberOfData(); i++)
		{
			mRegList[(i + mIt->second->m_pModbusADU->getqStartAddr())] = mIt->second->m_pModbusADU->mRegData[i];
			if (mIt->second->m_pModbusADU->getexceptionCode() == 2)
			{
				exceptionRequestReg.push_back(i + mIt->second->m_pModbusADU->getqStartAddr());
			}
		}
	}
	if (mIt->second->m_pModbusADU->getqFcode() == 4)
	{
		for (unsigned int i = 0; i < mIt->second->m_pModbusADU->getqNumberOfData(); i++)
		{
			mInputRegList[(i + mIt->second->m_pModbusADU->getqStartAddr())] = mIt->second->m_pModbusADU->mInputRegData[i];
			if (mIt->second->m_pModbusADU->getexceptionCode() == 2)
			{
				exceptionRequestInputReg.push_back(i + mIt->second->m_pModbusADU->getqStartAddr());
			}
		}
	}
}

void ModbusSlave::writeToSocket()
{
	if (mIt != mActiveRequests.end())
	{
		mIt->second->updateRequest();
		mIt->second->m_qByteTransferred = 0;
		mSocket.async_write_some(boost::asio::buffer(mIt->second->m_pModbusADU->qFrame.get(), mIt->second->m_pModbusADU->qFrameSize), [this](const boost::system::error_code& ec, std::size_t bytes_transferred) {
			if (ec.value() != 0)
			{
				mIt->second->mEc = ec;
				onRequestComplete();
				return;
			}
			if (mIt->second->mCancelledStatus)
			{
				onRequestComplete();
				return;
			}
			mIt->second->m_qByteTransferred += bytes_transferred;
			if (mIt->second->m_qByteTransferred == mIt->second->m_pModbusADU->qFrameSize)
			{
				mIt->second->m_aByteTransferred = 0;
				readFromSocket();
			}
		});
	}
}

void ModbusSlave::readFromSocket()
{
	mIt ->second ->m_pModbusADU ->aFrame.reset(new uint8_t[mIt->second->m_pModbusADU->aFrameSize]);
	mSocket.async_read_some(boost::asio::buffer(mIt->second->m_pModbusADU->aFrame.get(), mIt->second->m_pModbusADU->aFrameSize),
		std::bind(&ModbusSlave::readHandler, this, std::placeholders::_1, std::placeholders::_2, mIt->second->m_pModbusADU));
}

void ModbusSlave::readHandler(const boost::system::error_code & ec, std::size_t bytes_transferred, std::shared_ptr<ModbusADU> modbusADU)
{
	if (ec.value() != 0)
	{
		mIt->second->mEc = ec;
		onRequestComplete();
		return;
	}
	if (mIt->second->mCancelledStatus)
	{
		onRequestComplete();
		return;
	}
	mIt->second->m_aByteTransferred += bytes_transferred;
	if (mIt->second->m_aByteTransferred == mIt->second->m_pModbusADU->exceptionFrameSize)
	{
		onRequestComplete();
		return;
	}
	if (mIt->second->m_aByteTransferred == mIt->second->m_pModbusADU->aFrameSize) {
		onRequestComplete();
		return;
	}
	mSocket.async_read_some(boost::asio::buffer(modbusADU->aFrame.get() + bytes_transferred, modbusADU->aFrameSize - bytes_transferred),
		std::bind(&ModbusSlave::readHandler, this, std::placeholders::_1, std::placeholders::_2, modbusADU));
}

void ModbusSlave::onRequestComplete()
{
	mIt->second->m_pModbusADU->setResponseFrame();
	mIt->second->handleRequest();
	updateList();
	if (mIt->second->m_pModbusADU->getqFcode() == 5 or mIt->second->m_pModbusADU->getqFcode() == 6)
	{
		int requestId = mIt->second->mId;
		MBCancelRequest(requestId);
		//mIt = mActiveRequests.begin();
	}
	mIt++;
	writeToSocket();
}
