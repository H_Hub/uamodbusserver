#include "stdafx.h"
#include "ModbusTcpCon.h"

ModbusTcpCon::ModbusTcpCon()
{
	mWork.reset(new boost::asio::io_service::work(service));
	mThread.reset(new std::thread([this]() {
		service.run();
	}));
}

void ModbusTcpCon::modbusRun()
{
	mSlave->run();
}

void ModbusTcpCon::modbusClose()
{
	mWork.reset(NULL);
	mThread->join();
}
