#include "stdafx.h"
#include "ModbusADU.h"

ModbusADU::ModbusADU(const RequestInfo & requestInfo)
{
	qUnitID = requestInfo.mUnitId;
	qFcode = requestInfo.mFunctionCode;
	qStartAddr = requestInfo.mStartAdd;
	qNumberOfData = requestInfo.mNumberOfData;
	qLength = 12;
	if (qStartAddr < 0) illegaStatus = true;
}

void ModbusADU::setRequestFrame()
{
	if (qFcode == 1) setRequestFrameFC1();
	if (qFcode == 2) SetRequestFrameFC2();
	if (qFcode == 3) setRequestFrameFC3();
	if (qFcode == 4) SetRequestFrameFC4();
	if (qFcode == 5) setRequestFrameFC5();
	if (qFcode == 6) setRequestFrameFC6();
}

void ModbusADU::setResponseFrame()
{
	if (aFrame.get() != nullptr)
	{
		aTransID = 256 * (int)aFrame[0] + (int)aFrame[1];
		aLength = 256 * (int)aFrame[4] + (int)aFrame[5];
		aUnitID = (int)aFrame[6];
		aFCode = (int)aFrame[7];
		if (aTransID != qTransID) wrongStatus = true;
		if (aUnitID != qUnitID) timeoutStatus = true;
		if (aLength == 3 and aFCode == (128 + qFcode))
		{
			exceptionStatus =true;
			exceptionCode = (int)aFrame[8]; //byte following the functionCode
		}
		if ((timeoutStatus and wrongStatus and exceptionCode) == 0)
		{
			aNumberOfDatabytes = (int)aFrame[8];
			if (qFcode == 1)
			{
				std::unique_ptr<bool[]> tempCoilPtr = std::make_unique<bool[]>(aNumberOfDatabytes * 8);
				//convert data to bool
				for (int i = 0; i < aNumberOfDatabytes; i++)
				{
					int tempDatabyte = (int)(aFrame[9 + i]);
					for (int j = 0; j < 8; j++)
					{
						tempCoilPtr[8 * i + j] = (bool)(tempDatabyte % 2);
						tempDatabyte = tempDatabyte / 2;
					}
				}
				mCoilData = std::move(tempCoilPtr);
			}
			if (qFcode == 2)
			{
				std::unique_ptr<bool[]> tempInputCoilPtr = std::make_unique<bool[]>(aNumberOfDatabytes * 8);
				for (int i = 0; i < aNumberOfDatabytes; i++)
				{
					int tempDataByte = (int)(aFrame[9 + i]);
					for (int j = 0; j < 8; j++)
					{
						tempInputCoilPtr[8 * i + j] = (bool)(tempDataByte % 2);
						tempDataByte /= 2;
					}
				}
				mInputCoilData = std::move(tempInputCoilPtr);
			}
			if (qFcode == 3)
			{
				std::unique_ptr<int[]> tempRegPtr = std::make_unique<int[]>(qNumberOfData);
				for (int i = 0; i < aNumberOfDatabytes / 2; i++)
				{
					tempRegPtr[i] = 256 * (int)(aFrame[9 + 2 * i]) + (int)(aFrame[10 + 2 * i]);
				}
				mRegData = std::move(tempRegPtr);
			}
			if (qFcode == 4)
			{
				std::unique_ptr<int[]> tempInputRegPtr = std::make_unique<int[]>(qNumberOfData);
				for (int i = 0; i < aNumberOfDatabytes / 2; i++)
				{
					tempInputRegPtr[i] = 256 * (int)(aFrame[9 + 2 * i]) + (int)(aFrame[10 + 2 * i]);
				}
				mInputRegData = std::move(tempInputRegPtr);
			}
			
		}
	}
	return;
}

void ModbusADU::setRequestFrameFC1()
{
	qLength = 6;
	qFrameSize = 12;
	aFrameSize = 9 + qNumberOfData / 8 + 1;
	std::unique_ptr<uint8_t[]> tempPtr = std::make_unique<uint8_t[]>(qFrameSize);
	tempPtr[0] = (uint8_t)(qTransID >> 8);
	tempPtr[1] = (uint8_t)(qTransID);
	tempPtr[2] = (uint8_t)(qProtocolID >> 8);
	tempPtr[3] = (uint8_t)(qProtocolID);
	tempPtr[4] = (uint8_t)(qLength >> 8);
	tempPtr[5] = (uint8_t)(qLength);
	tempPtr[6] = (uint8_t)(qUnitID);
	tempPtr[7] = (uint8_t)(qFcode);
	tempPtr[8] = (uint8_t)(qStartAddr >> 8);
	tempPtr[9] = (uint8_t)(qStartAddr);
	tempPtr[10] = (uint8_t)(qNumberOfData >> 8);
	tempPtr[11] = (uint8_t)(qNumberOfData);

	qFrame = std::move(tempPtr);
}
void ModbusADU::SetRequestFrameFC2()
{
	qLength = 6;
	qFrameSize = 12;
	aFrameSize = 9 + qNumberOfData / 8 + 1;
	std::unique_ptr<uint8_t[]> tempPtr = std::make_unique<uint8_t[]>(qFrameSize);
	tempPtr[0] = (uint8_t)(qTransID >> 8);
	tempPtr[1] = (uint8_t)(qTransID);
	tempPtr[2] = (uint8_t)(qProtocolID >> 8);
	tempPtr[3] = (uint8_t)(qProtocolID);
	tempPtr[4] = (uint8_t)(qLength >> 8);
	tempPtr[5] = (uint8_t)(qLength);
	tempPtr[6] = (uint8_t)(qUnitID);
	tempPtr[7] = (uint8_t)(qFcode);
	tempPtr[8] = (uint8_t)(qStartAddr >> 8);
	tempPtr[9] = (uint8_t)(qStartAddr);
	tempPtr[10] = (uint8_t)(qNumberOfData >> 8);
	tempPtr[11] = (uint8_t)(qNumberOfData);
	qFrame = std::move(tempPtr);
}
void ModbusADU::setRequestFrameFC3()
{
	qLength = 6;
	qFrameSize = 12;
	aFrameSize = 9 + qNumberOfData * 2;
	std::unique_ptr<uint8_t[]> tempPtr = std::make_unique<uint8_t[]>(qFrameSize);
	tempPtr[0] = (uint8_t)(qTransID >> 8);
	tempPtr[1] = (uint8_t)(qTransID);
	tempPtr[2] = (uint8_t)(qProtocolID >> 8);
	tempPtr[3] = (uint8_t)(qProtocolID);
	tempPtr[4] = (uint8_t)(qLength >> 8);
	tempPtr[5] = (uint8_t)(qLength);
	tempPtr[6] = (uint8_t)(qUnitID);
	tempPtr[7] = (uint8_t)(qFcode);
	tempPtr[8] = (uint8_t)(qStartAddr >> 8);
	tempPtr[9] = (uint8_t)(qStartAddr);
	tempPtr[10] = (uint8_t)(qNumberOfData >> 8);
	tempPtr[11] = (uint8_t)(qNumberOfData);

	qFrame = std::move(tempPtr);
}
void ModbusADU::SetRequestFrameFC4()
{
	qLength = 6;
	qFrameSize = 12;
	aFrameSize = 9 + qNumberOfData * 2;
	std::unique_ptr<uint8_t[]> tempPtr = std::make_unique<uint8_t[]>(qFrameSize);
	tempPtr[0] = (uint8_t)(qTransID >> 8);
	tempPtr[1] = (uint8_t)(qTransID);
	tempPtr[2] = (uint8_t)(qProtocolID >> 8);
	tempPtr[3] = (uint8_t)(qProtocolID);
	tempPtr[4] = (uint8_t)(qLength >> 8);
	tempPtr[5] = (uint8_t)(qLength);
	tempPtr[6] = (uint8_t)(qUnitID);
	tempPtr[7] = (uint8_t)(qFcode);
	tempPtr[8] = (uint8_t)(qStartAddr >> 8);
	tempPtr[9] = (uint8_t)(qStartAddr);
	tempPtr[10] = (uint8_t)(qNumberOfData >> 8);
	tempPtr[11] = (uint8_t)(qNumberOfData);

	qFrame = std::move(tempPtr);
}
void ModbusADU::setRequestFrameFC5()
{
	qLength = 6;
	qFrameSize = 6 + qLength;
	aFrameSize = 12;

	std::unique_ptr<uint8_t[]> tempPtr = std::make_unique<uint8_t[]>(qFrameSize);
	tempPtr[0] = (uint8_t)(qTransID >> 8);
	tempPtr[1] = (uint8_t)(qTransID);
	tempPtr[2] = (uint8_t)(qProtocolID >> 8);
	tempPtr[3] = (uint8_t)(qProtocolID);
	tempPtr[4] = (uint8_t)(qLength >> 8);
	tempPtr[5] = (uint8_t)(qLength);
	tempPtr[6] = (uint8_t)(qUnitID);
	tempPtr[7] = (uint8_t)(qFcode);
	tempPtr[8] = (uint8_t)(qStartAddr >> 8);
	tempPtr[9] = (uint8_t)(qStartAddr);
	tempPtr[10] = (uint8_t)(qNumberOfData);
	tempPtr[11] = (uint8_t)(qNumberOfData >> 8);
	qFrame = std::move(tempPtr);
}
void ModbusADU::setRequestFrameFC6()
{
	qLength = 6;
	qFrameSize = 6 + qLength;
	aFrameSize = 12;

	std::unique_ptr<uint8_t[]> tempPtr = std::make_unique<uint8_t[]>(qFrameSize);
	tempPtr[0] = (uint8_t)(qTransID >> 8);
	tempPtr[1] = (uint8_t)(qTransID);
	tempPtr[2] = (uint8_t)(qProtocolID >> 8);
	tempPtr[3] = (uint8_t)(qProtocolID);
	tempPtr[4] = (uint8_t)(qLength >> 8);
	tempPtr[5] = (uint8_t)(qLength);
	tempPtr[6] = (uint8_t)(qUnitID);
	tempPtr[7] = (uint8_t)(qFcode);
	tempPtr[8] = (uint8_t)(qStartAddr >> 8);
	tempPtr[9] = (uint8_t)(qStartAddr);
	tempPtr[10] = (uint8_t)(qNumberOfData >> 8);
	tempPtr[11] = (uint8_t)(qNumberOfData);
	qFrame = std::move(tempPtr);
}
