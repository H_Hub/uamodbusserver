#pragma once
#ifdef MODBUSCLIENT_EXPORTS
#define MODBUSCLIENT_API __declspec(dllexport)
#else
#define MODBUSCLIENT_API __declspec(dllimport)
#endif

#include "ModbusRequest.h"
class MODBUSCLIENT_API ModbusSlave
{
public:
	ModbusSlave(boost::asio::io_service& service);
	~ModbusSlave() { mSocket.close();  mIt = mActiveRequests.begin();	};
	unsigned int  MBRead(RequestInfo& requestInfo);
	bool connect();
	void run();
	void MBCancelRequest(unsigned int requestID);
	void updateList();
	
	//serve UAopc
	std::map<int, bool> mCoilList;
	std::map<int, int> mRegList;
	std::map<int, bool> mInputCoilList;
	std::map<int, int> mInputRegList;
	std::vector<int> exceptionRequestReg;
	std::vector<int> exceptionRequestCoil;
	std::vector<int> exceptionRequestInputCoil;
	std::vector<int> exceptionRequestInputReg;
private:
	void writeToSocket();
	void readFromSocket();
	void readHandler(const boost::system::error_code& ec, std::size_t bytes_transferred, std::shared_ptr<ModbusADU> modbusADU);
	void onRequestComplete();

	boost::asio::ip::tcp::socket mSocket;
	boost::asio::ip::tcp::endpoint mEp;
	std::map<unsigned int, std::shared_ptr<ModbusRequest>> mActiveRequests;
	std::map<unsigned int, std::shared_ptr<ModbusRequest>>::iterator mIt = mActiveRequests.begin();
	boost::system::error_code mEc;
	boost::asio::deadline_timer mTimerSecondLoop;
	

	unsigned int  mIntervalTime;
	bool mConnectStatus;
	//bool mTimeoutStatus;

};