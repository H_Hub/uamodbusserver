#pragma once
#ifdef MODBUSCLIENT_EXPORTS
#define MODBUSCLIENT_API __declspec(dllexport)
#else
#define MODBUSCLIENT_API __declspec(dllimport)
#endif

#include "ModbusADU.h"

class MODBUSCLIENT_API ModbusRequest
{
public:
	ModbusRequest(const RequestInfo& requestInfo, std::shared_ptr<ModbusADU> modbusADU);
	virtual ~ModbusRequest() {};
	void updateRequest();
	void handleRequest();

private:
	bool mType;
	static unsigned int  sRequestCounter;
public:
	bool mValid;
	unsigned int mId; //gen and auto increate when update
	bool mCancelledStatus;
	std::size_t m_qByteTransferred;
	std::size_t m_aByteTransferred;
	std::shared_ptr<ModbusADU> m_pModbusADU;
	boost::system::error_code mEc;
};