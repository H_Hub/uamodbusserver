#include "stdafx.h"
#include "ModbusRequest.h"

unsigned int ModbusRequest::sRequestCounter = 0;
ModbusRequest::ModbusRequest(const RequestInfo & requestInfo, std::shared_ptr<ModbusADU> modbusADU)
	: m_pModbusADU(modbusADU), mId(++sRequestCounter)
{
}

void ModbusRequest::updateRequest()
{
	m_pModbusADU->qTransID++;
	m_pModbusADU->setRequestFrame();
}

void ModbusRequest::handleRequest()
{
	std::cout << "RequestID" << mId << ": ";
	if (mValid == 1) std::cout << "Valid" << "/ ";
	else
	{
		std::cout << "InValid" << "/ ";
	}
	if (mCancelledStatus == true) std::cout << "Cancelled" << "/ "; else std::cout << "Actived" << "/ ";
	if (mEc.value() != 0)
	{
		std::cout << "Error: " << mEc.message() << std::endl;
	}
	if (m_pModbusADU->getexceptionStatus() == 1)
	{
		std::cout << "Exception with code: " << m_pModbusADU->getexceptionCode() << std::endl;
	}
	else
	{
		std::cout << "No Exception" << std::endl;
	}
	if (m_pModbusADU->getqFcode() == 1)
	{

		if (m_pModbusADU->mCoilData.get() != nullptr)
		{
			for (unsigned int i = 0; i < m_pModbusADU->getqNumberOfData(); i++)
			{
				std::cout << "Coil[" << i + m_pModbusADU->getqStartAddr() << "] = " << std::boolalpha << m_pModbusADU->mCoilData[i] << "  ";	
			}
			std::cout<<std::endl;
		}
		else
		{
			std::cout << "No data to read" << std::endl;
		}
	}
	if (m_pModbusADU->getqFcode() == 2)
	{
		if (m_pModbusADU->mInputCoilData.get() != nullptr)
		{
			for (unsigned int i = 0; i < m_pModbusADU->getqNumberOfData(); i++)
			{
				std::cout << "InputCoil[" << i + m_pModbusADU->getqStartAddr() << "] = " << std::boolalpha << m_pModbusADU->mInputCoilData[i] << "  ";
				//mapInputCoilData.insert(std::pair<int, bool>(i + m_pModbusADU->qStartAddr, m_pModbusADU->mInputCoilData[i]));
			}
		}
		else
		{
			std::cout << "No data to read!" << std::endl;
		}
	}
	if (m_pModbusADU->getqFcode() == 3)
	{
		if (m_pModbusADU->mRegData.get() != nullptr)
		{
			for (unsigned int i = 0; i < m_pModbusADU->getqNumberOfData(); i++)
			{
				std::cout << "Reg[" << i + m_pModbusADU->getqStartAddr() << "] = " << m_pModbusADU->mRegData[i] << "  ";
			}
			std::cout<<std::endl;
		}
		else
		{
			std::cout << "No data to read" << std::endl;
		}
	}
	if (m_pModbusADU->getqFcode() == 4)
	{
		if (m_pModbusADU->mInputRegData.get() != nullptr)
		{
			for (unsigned int i = 0; i < m_pModbusADU->getqNumberOfData(); i++)
			{
				std::cout << "InputReg[" << i + m_pModbusADU->getqStartAddr() << "] = " << m_pModbusADU->mInputRegData[i] << "  ";
				//mapInputRegData.insert(std::pair<int, int>(i + m_pModbusADU->qStartAddr, m_pModbusADU->mInputRegData[i]));
			}
		}
		else { std::cout << "No data to read!" << std::endl; }
	}
	if (m_pModbusADU->getqFcode() == 5)
	{
		std::cout << "---Coil[" << m_pModbusADU->getqStartAddr() << "] be writen value: " << m_pModbusADU->getqNumberOfData() << "\n";
	}
	if (m_pModbusADU->getqFcode() == 6)
	{
		std::cout << "---Reg[" << m_pModbusADU->getqStartAddr() << "] be writen value: " << m_pModbusADU->getqNumberOfData() << "\n";
	}
	std::cout << std::endl;
}
