// writecoilDLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "writecoillib.h"


void run(boost::asio::io_service* service)
{
	service->run();
}

WriteCoil::WriteCoil() :sock(service), work(service)
{
	threadWork = std::thread(std::bind(&run, &service));
	
}
WriteCoil::~WriteCoil()
{
	sock.close();
	service.stop();
	threadWork.join();
}
void WriteCoil::connect(boost::asio::ip::tcp::endpoint & ep)
{
	memset(frameSender, 0, 12);
	frameSender[0] = 0;
	frameSender[1] = 1;
	frameSender[2] = 0;
	frameSender[3] = 0;
	frameSender[4] = 0;
	frameSender[5] = 6;
	frameSender[6] = 1;
	frameSender[7] = 6; // function code (input single register)
	frameSender[8] = (uint8_t)(addr >> 8);
	frameSender[9] = (uint8_t)(addr);
	frameSender[10] = (uint8_t)(num_val >> 8);
	frameSender[11] = (uint8_t)(num_val);


	sock.async_connect(ep, boost::bind(&WriteCoil::connectHandler, this, boost::asio::placeholders::error));
}
void WriteCoil::connectHandler(const boost::system::error_code & ec)
{

	if (!ec)
	{
		printf("CONNECTED TO SERVER\n");
		writeFrame();
	}
	else
	{
		printf("\nNo connection!\n");
	}
}
void WriteCoil::writeFrame()
{
	sock.async_write_some(boost::asio::buffer(frameSender, 12), boost::bind(&WriteCoil::writeFrameHanlder, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}
void WriteCoil::writeFrameHanlder(const boost::system::error_code & e, size_t byte)
{
	if (!e)
	{
		readCoil();
	}
	else
	{
		sock.close();
	}
}

void WriteCoil::readCoil()
{
	memset(response, 0, 1024);
	sock.async_read_some(boost::asio::buffer(response, 1024), boost::bind(&WriteCoil::readCoilHandler, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

}

void WriteCoil::readCoilHandler(const boost::system::error_code & ec, size_t byte)
{
	if (!ec)
	{
		const int size = static_cast<int>(response[8]);
		int rep[1024];// = new char[size];
		/// data chi tinh tu phan tu > response[8] ( 8 la so luong byte), nen copy tu phan tu thu 9 (refer Modbus)
		for (int i = 0, j = 9; i < size, j < size + 9; i++, j++)
		{
			rep[i] = response[j];
		}
		int m = 0;
		while (m < size)
		{
			repVector.push_back(rep[m] * 256 + (rep[m + 1]));
			m = m + 2;
		}
		/*for (auto k : repVector)
		{
			std::cout << k << " ";
		}
		std::cout << "\n";*/
	}
	else
	{
		std::cout << "ERROR";
	}
}

