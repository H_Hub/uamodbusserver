// mongodbDLL.cpp : Defines the exported functions for the DLL application.
//
//#include "stdafx.h"
#include "mongolib.h"


#include <mongocxx/instance.hpp>
#include <mongocxx/client.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>
#include <bsoncxx/types.hpp>
#include <mongocxx/uri.hpp>

using namespace std;
using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::open_document;

class MongoDB_pImpl
{
public:
	MongoDB_pImpl() {}
	~MongoDB_pImpl() {}
	static void init()
	{
		mongocxx::instance instance{};
	}
	bool connect()
	{
		uri = mongocxx::uri("mongodb://127.0.0.1:27017"); // this address can be configued by file config.cfg
		client = mongocxx::client(uri);
		db = client["tagModbusDB"];
		col = db["ModbusCoil"];
		colRegister = db["ModbusRegister"];
		colInputCoil = db["MBInputCoil"];
		colInputReg = db["MBInputReg"];
		return true;
	}
	void addRegCol(int32_t& i)
	{
		//colRegister = db["ModbusRegister"];
		auto builder = bsoncxx::builder::stream::document{};
		bsoncxx::document::value doc_value = builder << "Register" << i << bsoncxx::builder::stream::finalize;
		bsoncxx::document::view view = doc_value.view();
		colRegister.insert_one(view);
	}

	void addCol(int32_t& i)
	{
		//col = db["ModbusCoil"];
		auto builder = bsoncxx::builder::stream::document{};
		bsoncxx::document::value doc_value = builder << "coil" << i << bsoncxx::builder::stream::finalize;
		bsoncxx::document::view view = doc_value.view();
		col.insert_one(view);
	}
	void addInputCoil(int32_t& i)
	{
		auto builder = bsoncxx::builder::stream::document{};
		bsoncxx::document::value doc_value = builder << "InputCoil" << i << bsoncxx::builder::stream::finalize;
		bsoncxx::document::view view = doc_value.view();
		colInputCoil.insert_one(view);
	}
	void addInputReg(int32_t& i)
	{
		auto builder = bsoncxx::builder::stream::document{};
		bsoncxx::document::value doc_value = builder << "InputReg" << i << bsoncxx::builder::stream::finalize;
		bsoncxx::document::view view = doc_value.view();
		colInputReg.insert_one(view);
	}
	void delRegCol(int32_t& i)
	{
		auto builder = bsoncxx::builder::stream::document{};
		colRegister.delete_one(builder << "Register" << i << finalize);
	}
	void delCol(int32_t&i)
	{
		auto builder = bsoncxx::builder::stream::document{};
		col.delete_one(builder << "coil" << i << finalize);
	}
	void delInputCoil(int32_t& i)
	{
		auto builder = bsoncxx::builder::stream::document{};
		colInputCoil.delete_one(builder << "InputCoil" << i << finalize);
	}
	void delInputReg(int32_t& i)
	{
		auto builder = bsoncxx::builder::stream::document{};
		colInputReg.delete_one(builder << "InputReg" << i << finalize);
	}

	void updateCoil(std::vector<int32_t>& coil)
	{
		auto cursor = col.find({}); //find all
		for (auto&& doc : cursor)
		{
			element = doc["coil"];
			if (element && element.type() == bsoncxx::type::k_int32)
			{
				coil.push_back(element.get_int32().value);
			}
		}
	}

	void updateReg(std::vector<int32_t>& reg)
	{
		auto cursor = colRegister.find({}); //find all
		for (auto&& doc : cursor)
		{
			element = doc["Register"];
			if (element && element.type() == bsoncxx::type::k_int32)
			{
				reg.push_back(element.get_int32().value);
			}
		}
	}
	void updateInputCoil(std::vector<int32_t>& inputCoil)
	{
		auto cursor = colInputCoil.find({}); //find all
		for (auto&& doc : cursor)
		{
			element = doc["InputCoil"];
			if (element && element.type() == bsoncxx::type::k_int32)
			{
				inputCoil.push_back(element.get_int32().value);
			}
		}
	}
	void updateInputReg(std::vector<int32_t>& inputReg)
	{
		auto cursor = colInputReg.find({}); //find all
		for (auto&& doc : cursor)
		{
			element = doc["InputReg"];
			if (element && element.type() == bsoncxx::type::k_int32)
			{
				inputReg.push_back(element.get_int32().value);
			}
		}
	}

private:
	mongocxx::uri uri;
	mongocxx::client client;
	mongocxx::database db;
	mongocxx::collection col;
	mongocxx::collection colRegister;
	mongocxx::collection colInputCoil;
	mongocxx::collection colInputReg;
	bsoncxx::document::element element;
};

MongoDB::MongoDB() : m_pImpl(new MongoDB_pImpl())
{
	
}

MongoDB::~MongoDB()
{
}

void MongoDB::initDB()
{
	//m_pImpl->init();
	MongoDB_pImpl::init();
}

bool MongoDB::connectDB()
{
	m_pImpl->connect();
	return true;
}

void MongoDB::addRegColDB(int32_t i)
{
	m_pImpl->addRegCol(i);
}

void MongoDB::addColDB(int32_t i)
{
	m_pImpl->addCol(i);
}

void MongoDB::delRegColDB(int32_t i)
{
	m_pImpl->delRegCol(i);
}

void MongoDB::delColDB(int32_t i)
{
	m_pImpl->delCol(i);
}

void MongoDB::pushCoil(std::vector<int32_t>&coil) //refer because vector will be changed 
{
	m_pImpl->updateCoil(coil);
}

void MongoDB::pushReg(std::vector<int32_t>&reg)
{
	m_pImpl->updateReg(reg);
}

void MongoDB::addInputCoil(int32_t i)
{
	m_pImpl->addInputCoil(i);
}

void MongoDB::addInputReg(int32_t i)
{
	m_pImpl->addInputReg(i);
}

void MongoDB::delInputCoil(int32_t i)
{
	m_pImpl->delInputCoil(i);
}

void MongoDB::delInputReg(int32_t i)
{
	m_pImpl->delInputReg(i);
}

void MongoDB::pushInputCoil(std::vector<int32_t>& inputCoil)
{
	m_pImpl->updateInputCoil(inputCoil);
}

void MongoDB::pushInputReg(std::vector<int32_t>& inputReg)
{
	m_pImpl->updateInputReg(inputReg);
}

