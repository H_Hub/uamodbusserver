#pragma once
#include "nmbuilding.h"
#include <uaobjecttypes.h>
#include <nodemanagerbase.h>
#include <userdatabase.h>
#include <methodmanager.h>
#include <opcua_dataitemtype.h>


class NmBuilding;
class UaMethodGeneric;//<-- method support


class ModbusObject : public UaObjectBase//, public MethodManager  //this is important for method support
{
	UA_DISABLE_COPY(ModbusObject);

public:
	ModbusObject(const UaString& name, const UaNodeId& newNodeId, const UaString& defaultLocleId, NmBuilding* pNodeManager, OpcUa_UInt32 deviceAddress);
	virtual ~ModbusObject(void);
	OpcUa::DataItemType* pDataItem = NULL;
	OpcUa_Byte eventNotifier() const;

	//Override Uaobject method implementation
	//MethodManager* getMethodManager(UaMethod* pMethod) const override;
	//Implement MethodManager Interface	
	/*virtual UaStatus beginCall(MethodManagerCallback* pCallback, const ServiceContext& serviceContext, OpcUa_UInt32 callbackHandle, MethodHandle* pMethodHandle, const UaVariantArray& inputArguments);	*/
	virtual UaStatus call(UaMethod* /*pMethod*/, 
		const UaVariantArray&/*inputArgument*/,
		UaVariantArray&	,
		UaStatusCodeArray&/*inputArgumentResults*/,
		UaDiagnosticInfos&//,
		//NmBuilding * pNodeManager
		) // them param pNodeManager de dung cho tagmodbusobject.h/cpp, lay cac ham cua Nmuilding* pNodeManager
	{
		return OpcUa_BadMethodInvalid;
	}


protected:
	UaMutexRefCounted* m_pSharedMutex;
	OpcUa_UInt32 m_deviceAddress;

private:
	//NmBuilding* m_pNodeManager;
	UaMethodGeneric* m_pMethodAdd;

};

class BaUserData : public UserDataBase
{
	UA_DISABLE_COPY(BaUserData);
public:
	BaUserData()
	{
	}
	virtual ~BaUserData() {}

};
