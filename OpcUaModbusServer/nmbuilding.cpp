﻿//#include "tagmongodb.h"
#include "nmbuilding.h"
#include "tagmodbusobject.h"
#include "modbusobject.h"

class TagModbusObject;

class BaseVariableType : public OpcUa::BaseDataVariableType
{
public: 
	BaseVariableType(
		const UaNodeId&    nodeId,
		const UaString&    name,
		OpcUa_UInt16       browseNameNameSpaceIndex,
		const UaVariant&   initialValue,
		OpcUa_Byte         accessLevel,
		NodeManagerConfig* pNodeConfig,
		UaMutexRefCounted* pSharedMutex = NULL)
		: OpcUa::BaseDataVariableType(nodeId, name, browseNameNameSpaceIndex, initialValue, accessLevel, pNodeConfig, pSharedMutex), m_address(0)
	{}
	virtual ~BaseVariableType() {}
	void setAddress(int address) { /// used when query data from mongodb
		m_address = address;
	}
	int getAddress() const {
		return m_address;
	}
	virtual int  getVariableType() = 0; // pure virtual	

private:
	int m_address;
};
class CoilVariableType : public BaseVariableType//public OpcUa::BaseDataVariableType // Xay dung 1 class de luu tru cac setting
{
public:
	CoilVariableType(
		const UaNodeId&    nodeId,
		const UaString&    name,
		OpcUa_UInt16       browseNameNameSpaceIndex,
		const UaVariant&   initialValue,
		OpcUa_Byte         accessLevel,
		NodeManagerConfig* pNodeConfig,
		UaMutexRefCounted* pSharedMutex = NULL)
		: BaseVariableType(nodeId, name, browseNameNameSpaceIndex, initialValue, accessLevel, pNodeConfig, pSharedMutex), m_address(0)
	{}
	
	virtual ~CoilVariableType() {}
	UaNodeId typeDefinitionId() const override
	{
		UaNodeId id(10001, 2);		
		return id;
	};
	virtual int  getVariableType() override
	{
		return 1 ; //1 = CoilVariableType
	};

private:
	int m_address; 
};
class RegVariableType : public BaseVariableType
{
public:
	RegVariableType(
		const UaNodeId&    nodeId,
		const UaString&    name,
		OpcUa_UInt16       browseNameNameSpaceIndex,
		const UaVariant&   initialValue,
		OpcUa_Byte         accessLevel,
		NodeManagerConfig* pNodeConfig,
		UaMutexRefCounted* pSharedMutex = NULL)
		: BaseVariableType(nodeId, name, browseNameNameSpaceIndex, initialValue, accessLevel, pNodeConfig, pSharedMutex), m_address(0)
	{}	
	virtual ~RegVariableType() {}
	
	virtual int getVariableType() override
	{
		return 2; // 2 = RegVariableType
	}
	UaNodeId typeDefinitionId() const override
	{
		UaNodeId id(10002, 2);
		return id;
	};

private:
	int m_address;

};
class InputCoilVariableType : public BaseVariableType
{
public:
	InputCoilVariableType(

		const UaNodeId&    nodeId,
		const UaString&    name,
		OpcUa_UInt16       browseNameNameSpaceIndex,
		const UaVariant&   initialValue,
		OpcUa_Byte         accessLevel,
		NodeManagerConfig* pNodeConfig,
		UaMutexRefCounted* pSharedMutex = NULL)
		: BaseVariableType(nodeId, name, browseNameNameSpaceIndex, initialValue, accessLevel, pNodeConfig, pSharedMutex), m_address(0)
	{}
	virtual ~InputCoilVariableType() {}
	virtual int getVariableType() override
	{
		return 3; // defined 3= InputCoil
	}
	UaNodeId typeDefinitionId() const override
	{
		UaNodeId id(10003, 2);
		return id;
	}
private:
	int m_address;
};
class InputRegVariableType : public BaseVariableType
{
public:
	InputRegVariableType(
		const UaNodeId&    nodeId,
		const UaString&    name,
		OpcUa_UInt16       browseNameNameSpaceIndex,
		const UaVariant&   initialValue,
		OpcUa_Byte         accessLevel,
		NodeManagerConfig* pNodeConfig,
		UaMutexRefCounted* pSharedMutex = NULL)
		: BaseVariableType(nodeId, name, browseNameNameSpaceIndex, initialValue, accessLevel, pNodeConfig, pSharedMutex), m_address(0)
	{}
	virtual ~InputRegVariableType() {}
	virtual int getVariableType() override
	{
		return 4; // defined 4 = InputReg
	}
	UaNodeId typeDefinitionId() const override
	{
		UaNodeId id(10004, 2);
		return id;
	}
private:
	int m_address;
};
NmBuilding::NmBuilding() : NodeManagerBase("urn:HVH:OpcUaModbusSerer:BuildingOpcUaModbusServer")
{
	
}

NmBuilding::~NmBuilding()
{
	client.modbusClose();
}

UaStatus NmBuilding::afterStartUp()
{
	UaStatus ret;
	TagModbusObject * pTagModbus =NULL;
	UaString sName;

	OpcUa_Int32 i;
	UaString sModbusName;
	OpcUa_UInt32 count = 1 ;
	NmBuilding* pNodeManager;
	OpcUa_UInt32 modbusAddress;	
	UaThread::sleep(2); // Ensure Modbus Data exist to serve OPCUA server
	createTypeNode();
	///-Create folder "ModbusOpcUa" and add the folder to the ObjectFolder
	pFolder = new UaFolder("ModbusOpcUa", UaNodeId("ModbusOpcUa", getNameSpaceIndex()), m_defaultLocaleId);
	ret = addNodeAndReference(OpcUaId_ObjectsFolder, pFolder, OpcUaId_HasChild);	
	
	for (i = 0; i < count; i++)
	{
		ret = getModbusConfig(i, sModbusName, modbusAddress); //  "getModbusConfig" be used to get name for sModbusName ("TagModbusObject")
		pTagModbus = new TagModbusObject(
			sModbusName,
			UaNodeId(sModbusName, getNameSpaceIndex()),
			m_defaultLocaleId,
			this,
			modbusAddress);
		ret = addNodeAndReference(pFolder, pTagModbus, OpcUaId_HasComponent);
		UA_ASSERT(ret.isGood());
	}
	pMongoDB->connectDB();
	pMongoDB->pushCoil(coil);
	//Add tag//Add node from mongodb
	for (auto j : coil)
	{
		CoilVariableType* pDataVariable;
		//OpcUa::DataItemType* pDataItem;
		UaVariant defaultValue;
		UaStatus addStatus;
		//Add Variable "AddedNode" as BaseDataVariable		
	
		auto iter = slave->mCoilList.find(j);
		if (iter != std::end(slave->mCoilList))
		{
			defaultValue.setBool(iter->second);			
		}
		else
		{
			defaultValue.setBool(false);
		
		}

		UaString sDisplayName = UaString("TagModbus_Coil%1").arg(j);
		UaString sNodeId = UaString("coil%1").arg(j);
		pDataVariable = new CoilVariableType(
			UaNodeId(sNodeId, getNameSpaceIndex()),
			sDisplayName,
			getNameSpaceIndex(),
			defaultValue,
			Ua_AccessLevel_CurrentRead | Ua_AccessLevel_CurrentWrite,
			this);//Node manager for this variable (pDataVariableTag)
		pDataVariable->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
		pDataVariable->setValueHandling(UaVariable_Value_CacheIsSource);
		addStatus = addNodeAndReference(pFolder, pDataVariable, OpcUaId_HasComponent);
		UA_ASSERT(addStatus.isGood());

		///get param, setting by get(), set() method
		pDataVariable->setAddress(j);

	}
	
	pMongoDB->pushReg(reg);
	for (auto k : reg)
	{
		RegVariableType* pDataVariable;
		UaVariant value;		
		//value.setInt32(pModbusTcp->repReg[k]);
		auto iter = slave->mRegList.find(k);
		if (iter != std::end(slave->mRegList))
		{
			value.setInt32((int32_t)iter->second);
		}
		else value.setInt32(0);
		UaStatus addStatus;
		UaString sDisplayName = UaString("TagModbus_Register%1").arg(k);
		UaString sNodeId = UaString("reg%1").arg(k);
		pDataVariable = new RegVariableType(
			UaNodeId(sNodeId, getNameSpaceIndex()),
			sDisplayName,
			getNameSpaceIndex(),
			value,
			Ua_AccessLevel_CurrentRead | Ua_AccessLevel_CurrentWrite,
			this);//Node manager for this variable (pDataVariableTag)
		pDataVariable->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
		pDataVariable->setValueHandling(UaVariable_Value_CacheIsSource);
		addStatus = addNodeAndReference(pFolder, pDataVariable, OpcUaId_HasComponent);
		UA_ASSERT(addStatus.isGood());
		pDataVariable->setAddress(k);
	}
	
	pMongoDB->pushInputReg(inputReg);
	if (inputReg.size() != 0)
	{
		for (auto iterInputReg : inputReg)
		{
			InputRegVariableType* pDataVariable;
			UaVariant value;
			auto iter = slave->mInputRegList.find(iterInputReg);
			if (iter != std::end(slave->mInputRegList))
			{
				value.setInt32((int32_t)iter->second);
			}
			else value.setInt32(0);
			UaStatus addStatus;
			UaString sDisplayName = UaString("Input_Register%1").arg(iterInputReg);
			UaString sNodeId = UaString("inputreg%1").arg(iterInputReg);
			pDataVariable = new InputRegVariableType(
				UaNodeId(sNodeId, getNameSpaceIndex()),
				sDisplayName,
				getNameSpaceIndex(),
				value,
				Ua_AccessLevel_CurrentRead,// | Ua_AccessLevel_CurrentWrite,
				this);//Node manager for this variable (pDataVariableTag)
			pDataVariable->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
			pDataVariable->setValueHandling(UaVariable_Value_CacheIsSource);
			addStatus = addNodeAndReference(pFolder, pDataVariable, OpcUaId_HasComponent);
			UA_ASSERT(addStatus.isGood());
			pDataVariable->setAddress(iterInputReg);
		}
	}
	
	return ret;
}

UaStatus NmBuilding::beforeShutDown()
{
	UaStatus ret;
	return ret;
}

//Using for "UaVariable_Value_Cache"
UaStatus NmBuilding::readValues()
{
	UaStatus ret;
	UaVariable* pInstanceDeclaration = NULL;
	pInstanceDeclaration = getInstanceDeclarationVariable("point");
	UaVariant uaCoil0;	
	uaCoil0.setBool(false);
	UaDataValue dt0(uaCoil0, OpcUa_Good, UaDateTime::now(), UaDateTime::now());
	pInstanceDeclaration->setValue(NULL, dt0, OpcUa_False);

	//Variable "statuscoil" of TagModbusObject	
	pInstanceDeclaration = getInstanceDeclarationVariable("statuscoil");	
	UaVariant uaCoil1;	
	uaCoil1.setBool(true);
	UaDataValue dt1(uaCoil1, OpcUa_Good, UaDateTime::now(), UaDateTime::now());
	pInstanceDeclaration->setValue(NULL, dt1, OpcUa_False); 
	
	for (auto c : coil)
	{
		UaString sCoilId = UaString("coil%1").arg(c);
		pInstanceDeclaration = getInstanceDeclarationVariable(sCoilId);
		UaVariant uaCoil;	
		UaDataValue dt2;
		if (slave->exceptionRequestCoil.size() != 0)
		{
			for (auto it : slave->exceptionRequestCoil)
			{
				if (c == it)
				{
					uaCoil.setBool(false);
					dt2 = UaDataValue(uaCoil, OpcUa_Bad, UaDateTime::now(), UaDateTime::now());
					pMongoDB->delColDB(c); // not store this point to mongodb
				}
				else
				{
					auto iter = slave->mCoilList.find(c);
					if (iter != std::end(slave->mCoilList))
					{
						uaCoil.setBool(iter->second);
						dt2 = UaDataValue(uaCoil, OpcUa_Good, UaDateTime::now(), UaDateTime::now());
					}
				}
			}
		}
		else
		{
			auto iter = slave->mCoilList.find(c);
			if (iter != std::end(slave->mCoilList))
			{
				uaCoil.setBool(iter->second);
				dt2 = UaDataValue(uaCoil, OpcUa_Good, UaDateTime::now(), UaDateTime::now());
			}
		}
		
		pInstanceDeclaration->setValue(NULL, dt2, OpcUa_False);
	}
	for (auto r : reg)
	{
		UaString sRegId = UaString("reg%1").arg(r);
		pInstanceDeclaration = getInstanceDeclarationVariable(sRegId);
		UaVariant uaReg;
		UaDataValue dt3;
		if (slave->exceptionRequestReg.size() != 0)
		{
			for (auto it : slave->exceptionRequestReg)
			{
				if (r == it)
				{
					uaReg.setInt32(0);
					dt3 = UaDataValue(uaReg, OpcUa_Bad, UaDateTime::now(), UaDateTime::now());
					pMongoDB->delRegColDB(r); // not store this point in mongodb
				}
				else
				{
					auto iter = slave->mRegList.find(r);
					if (iter != std::cend(slave->mRegList)) {
						uaReg.setInt32((int32_t)iter->second);
						dt3 = UaDataValue(uaReg, OpcUa_Good, UaDateTime::now(), UaDateTime::now());
					}
				}
			}
		}
		else
		{
			auto iter = slave->mRegList.find(r);
			if (iter != std::cend(slave->mRegList)) {
				uaReg.setInt32((int32_t)iter->second);
				dt3 = UaDataValue(uaReg, OpcUa_Good, UaDateTime::now(), UaDateTime::now());
			}
		}
		
		pInstanceDeclaration->setValue(NULL, dt3, OpcUa_False);

	}			
	for (auto iterInputReg : inputReg)
	{
		UaString sInputRegId = UaString("inputreg%1").arg(iterInputReg);
		pInstanceDeclaration = getInstanceDeclarationVariable(sInputRegId);
		UaVariant uaInputReg;
		UaDataValue dataInputReg;
		if (slave->exceptionRequestInputReg.size() != 0) // for requests  have exception
		{
			for (auto it : slave->exceptionRequestInputReg)
			{
				if (iterInputReg == it)
				{
					uaInputReg.setInt32(0);
					dataInputReg = UaDataValue(uaInputReg, OpcUa_Bad, UaDateTime::now(), UaDateTime::now());
					pMongoDB->delInputReg(iterInputReg);
				}
				else
				{
					auto iter = slave->mInputRegList.find(iterInputReg);
					if (iter != slave->mInputRegList.end())
					{
						uaInputReg.setInt32((int32_t)iter->second);
						dataInputReg = UaDataValue(uaInputReg, OpcUa_Good, UaDateTime::now(), UaDateTime::now());
					}
				}
			}
		}
		else // for requests do not have exception
		{
			auto iter = slave->mInputRegList.find(iterInputReg);
			if (iter != slave->mInputRegList.end())
			{
				uaInputReg.setInt32((int32_t)iter->second);
				dataInputReg = UaDataValue(uaInputReg, OpcUa_Good, UaDateTime::now(), UaDateTime::now());
			}
		}
		pInstanceDeclaration->setValue(NULL, dataInputReg, OpcUa_False);
	}
	return ret;
}

OpcUa_Boolean NmBuilding::beforeSetAttributeValue(Session * pSession, UaNode * pNode, OpcUa_Int32 attributeId, const UaDataValue & dataValue, OpcUa_Boolean & checkWriteMask)
{
	
	//separate which subclass is used( coil or Register). indentifierNumeric
	OpcUa_UInt32 typeDefinitionId = pNode->typeDefinitionId().identifierNumeric();

	BaseVariableType* pBaseVaribleType= static_cast<BaseVariableType*> (pNode);
	int type = pBaseVaribleType->getVariableType();
	
	if (type == 1)
	{
		CoilVariableType* pCoilVariableType = static_cast<CoilVariableType*> (pNode);
		int address = pCoilVariableType->getAddress();
		bool value = dataValue.value()->Value.Boolean;
		int valueInt;
		if (value == false) { valueInt = 0; }
		else { valueInt = 255; }
		RequestInfo reqInfo = RequestInfo(1, 5, (uint8_t)address, (uint8_t)valueInt);
		unsigned int  requestId = slave->MBRead(reqInfo);
	}
	if (type ==2)
	{
		RegVariableType* pRegVariableType = static_cast<RegVariableType*>(pNode);
		int address = pRegVariableType->getAddress();
		int value = dataValue.value()->Value.Int32;
		RequestInfo reqInfo = RequestInfo(1, 6, (uint8_t)address, (uint8_t)value);
		slave->MBRead(reqInfo);
		unsigned int  requestId = slave->MBRead(reqInfo);
	}
	
	return OpcUa_True;
}

UaVariable * NmBuilding::getInstanceDeclarationVariable(UaString stringIdentifier)
{	
	UaNode* pNode = findNode(UaNodeId(stringIdentifier, getNameSpaceIndex()));
	if ((pNode != NULL) && (pNode->nodeClass() == OpcUa_NodeClass_Variable))
	{
		return (UaVariable*)pNode;
	}
	else
	{
		return NULL;
	}
}

UaStatus NmBuilding::createTypeNode()
{
	UaStatus ret;
	UaStatus addStatus;

	UaObjectTypeSimple* pOpcModbusType =NULL;
	UaObjectTypeSimple* pTagModbusType = NULL;
	UaVariant defaultValue;
	
	OpcUa::BaseDataVariableType* pDataVariable;
	OpcUa::DataItemType* pDataItem;
	
	// CREATE THE OPCMODBUS TYPE
	//Add object OPCModbusType
	pOpcModbusType = new UaObjectTypeSimple(
		"OpcModbusType",
		UaNodeId("opcmodbustype", getNameSpaceIndex()),
		m_defaultLocaleId,
		OpcUa_True);	
	addStatus = addNodeAndReference(OpcUaId_BaseObjectType, pOpcModbusType, OpcUaId_HasSubtype);
	UA_ASSERT(addStatus.isGood());

	/// CREATE THE OPCMODBUS TYPE Instance Declaration
	//Add Variable "Tag" as BaseDataVariable
	defaultValue.setBool(false);
	pDataVariable = new OpcUa::BaseDataVariableType(
		UaNodeId("tag", getNameSpaceIndex()),
		"Tag",
		getNameSpaceIndex(),
		defaultValue,
		Ua_AccessLevel_CurrentRead | Ua_AccessLevel_CurrentWrite,
		this);//Node manager for this variable (pDataVariableTag)
	pDataVariable->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
	pDataVariable->setValueHandling(UaVariable_Value_CacheIsSource);
	addStatus = addNodeAndReference(pOpcModbusType, pDataVariable, OpcUaId_HasComponent);
	UA_ASSERT(addStatus.isGood());


	//CREATE THE TAGMODBUS TYPE

	pTagModbusType = new UaObjectTypeSimple(
		"TagModbusType",
		UaNodeId("tagmodbustype",getNameSpaceIndex()),
		m_defaultLocaleId,
		OpcUa_False	);
	addStatus = addNodeAndReference(pOpcModbusType, pTagModbusType, OpcUaId_HasSubtype);
	UA_ASSERT(addStatus.isGood());
	///create instance declaration of TagModbus type
	
	///ADD POINT HERE
	//Add variable "Point" 
	defaultValue.setBool(true);
	pDataItem = new  OpcUa::DataItemType (
		UaNodeId("point", getNameSpaceIndex()),
		"Point",
		getNameSpaceIndex(),
		defaultValue,
		(Ua_AccessLevel_CurrentRead |Ua_AccessLevel_CurrentWrite),
		this);//Node manager for this variable (pDataVariableTag)
	pDataItem->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
	addStatus = addNodeAndReference(pTagModbusType, pDataItem, OpcUaId_HasComponent);
	UA_ASSERT(addStatus.isGood());

	pDataItem->setValueHandling(UaVariable_Value_CacheIsSource);
	//checking datachange?- > if (datachange) -> update by implement setValue()/
	//pDataItem->setValue(nullptr, dataValue, OpcUa_False);	

	//add variable "Voltage"
	defaultValue.setInt32(10);
	pDataItem = new  OpcUa::DataItemType(
		UaNodeId("voltage", getNameSpaceIndex()),
		"Voltage",
		getNameSpaceIndex(),
		defaultValue,
		(Ua_AccessLevel_CurrentWrite  | Ua_AccessLevel_CurrentRead),
		this);//Node manager for this variable (pDataVariableTag)
	pDataItem->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);
	pDataItem->setValueHandling(UaVariable_Value_CacheIsSource); //chinh la kieu mac dinh cua OPCUA la UaVriable_Value_CacheIsSource
	addStatus = addNodeAndReference(pTagModbusType, pDataItem, OpcUaId_HasComponent);
	UA_ASSERT(addStatus.isGood());

	//add variable "statuscoil"
	defaultValue.setBool(true);
	pDataItem = new  OpcUa::DataItemType(
		UaNodeId("statuscoil", getNameSpaceIndex()),
		"StatusCoil",
		getNameSpaceIndex(),
		defaultValue,
		Ua_AccessLevel_CurrentRead,
		this);//Node manager for this variable (pDataVariableTag)
		pDataItem->setModellingRuleId(OpcUaId_ModellingRule_Mandatory);			
	pDataItem->setValueHandling(UaVariable_Value_CacheIsSource);	
	addStatus = addNodeAndReference(pTagModbusType, pDataItem, OpcUaId_HasComponent);
	UA_ASSERT(addStatus.isGood());
	
	return ret;
}
UaStatusCode NmBuilding::getModbusConfig(OpcUa_UInt32 index, UaString & sName, OpcUa_UInt32 & address)
{
	address = index + 1;
	char name[100];
	sprintf(name,"TagOption_%d",address);
	sName = name;

	return OpcUa_Good;
}
