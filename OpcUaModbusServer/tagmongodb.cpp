#include "tagmongodb.h"

void connectDB()
{
	mongocxx::uri uri1("mongodb://127.0.0.1:27017"); // this address can be configued by file config.cfg
	mongocxx::client client1(uri1);
	mongocxx::database db1 = client1["tagModbusDB"];
	mongocxx::collection col1 = db1["ModbusCoil"];
	mongocxx::collection colRegister1 = db1["ModbusRegister"];
}
