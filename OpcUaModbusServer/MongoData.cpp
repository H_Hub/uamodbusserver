#include "MongoData.h"

MongoDataBase::MongoDataBase()
{
	m_pMongoDB = new MongoDB();
	m_pMongoDB->initDB();
	m_pMongoDB->connectDB();
	m_pMongoDB->pushCoil(mCoilVector);
	m_pMongoDB->pushReg(mRegVector);
	m_pMongoDB->pushInputCoil(mInputCoilVector);
	m_pMongoDB->pushInputReg(mInputRegVector);
}

void MongoDataBase::getDataMongo()
{
	updateMap(mCoilVector, mListCoil);
	updateMap(mRegVector, mListReg);
	updateMap(mInputCoilVector, mListInputCoil);
	updateMap(mInputRegVector, mListInputReg);
}

void MongoDataBase::updateMap(std::vector<int32_t>& vector, std::map<int, int>& map)
{
	//ensure always exist value from vector
	if (vector.size() == 0)
	{
		std::cout << "Exits DataType no value" << std:: endl;
		return;
	}

	std::sort(vector.begin(), vector.end());
	std::vector<std::vector<int>> subVector(1, std::vector<int>(1, vector[0]));
	std::for_each(vector.begin() + 1, vector.end(), [&](int i)
	{
		if (subVector.back().back() == i - 1)
			subVector.back().push_back(i);
		else
		{
			subVector.push_back(std::vector<int>(1, i));
		}
	});
	for (auto const& i : subVector)
	{
		map[*(i.begin())] = i.size();
	}
}
