
#include "nmbuilding.h"
#include <opcserver.h>
#include <xmldocument.h>
#include <uaplatformdefs.h>
#include <uaplatformlayer.h>
#include <uathread.h>
#include <iostream>
#include <shutdown.h>

#include <uathread.h>
#include <conio.h>
#include <mutex>
#include <vector>

std::mutex mutex;

int OpcServerMain(const char* szAppPath)
{
	int ret = 0;
	UaXmlDocument::initParser();
	ret = UaPlatformLayer::init();
	if (ret == 0)
	{
		UaString sConfigFileName(szAppPath);
		sConfigFileName += "/ServerConfig.xml";
		OpcServer* pServer = new OpcServer;
		pServer->setServerConfig(sConfigFileName, szAppPath);
		
		///Tao 1 node model moi
		NmBuilding *pMyNodeManager = new NmBuilding();
		pServer->addNodeManager(pMyNodeManager);
		std::thread thr2([&pServer, &ret, &pMyNodeManager]() {
			ret = pServer->start();
			if (ret != 0)
			{
				delete pServer;
				pServer = 0;
			}
			if (ret == 0)
			{
				//unique_lock<mutex> lock2(m);
				std::cout << "\n*************************************\n";
				printf("Press %s to shutdown server\n", SHUTDOWN_SEQUENCE);
				std::cout << "*************************************\n";
				
				while (ShutDownFlag() == 0)
				{					
					UaThread::msleep(1000); 
					
					pMyNodeManager->readValues();
					
				}
				std::cout << "*************************************\n";
				std::cout << "Shutting down Server\n";
				std::cout << "*************************************\n";
				///lock2.unlock();

				//-Stop OPC server--------------
				//Stop server and  wait 2 seconds if clients are connected 
				//to allow them to disconnect after  they received the shutdown signal
				pServer->stop(2, UaLocalizedText("", "User shutdown"));
				delete pServer;
				pServer = NULL;
				
			}
		}
		);
		///for MongoDB
		//pMyNodeManager->pMongoDB->initDB();
		//pMyNodeManager->pMongoDB->connectDB();		

		/*boost::asio::ip::tcp::endpoint ep(boost::asio::ip::address_v4::from_string("127.0.0.1"), 502);		
		pMyNodeManager->pModbusTcp->connect(ep);
		pMyNodeManager->pCoil->readCoil();
		pMyNodeManager->pRegister->readRegister();
		pMyNodeManager->service.run();*/
				
		pMyNodeManager->slave->connect();
		pMyNodeManager->mongoDB.getDataMongo();
		for (auto it = pMyNodeManager->mongoDB.mListCoil.begin(); it != pMyNodeManager->mongoDB.mListCoil.end(); ++it)
		{
			int startAddr = it->first;
			int numberOfData = it->second;
			RequestInfo reqInfo = RequestInfo(1, 1, startAddr, numberOfData);
			pMyNodeManager->slave->MBRead(reqInfo);
		}
		for (auto it = pMyNodeManager->mongoDB.mListReg.begin(); it != pMyNodeManager->mongoDB.mListReg.end(); ++it)
		{
			int startAddr = it->first;
			int numberOfData = it->second;
			RequestInfo reqInfo = RequestInfo(1, 3, startAddr, numberOfData);
			pMyNodeManager->slave->MBRead(reqInfo);
		}
		for (auto it = pMyNodeManager->mongoDB.mListInputReg.begin(); it != std::cend(pMyNodeManager->mongoDB.mListInputReg); ++it)
		{
			int startAddr = it->first;
			int number = it->second;
			RequestInfo reqInfo = RequestInfo(1, 4, startAddr, number);
			pMyNodeManager->slave->MBRead(reqInfo);
		}
		pMyNodeManager->slave->run();
		pMyNodeManager->client.modbusClose();

		thr2.join();		
		
	}
	UaPlatformLayer::cleanup();
	UaXmlDocument::cleanupParser();
	return ret;
}
int main()
{	
	///mongocxx::instance instance{}; // have to
	int ret = 0;
	RegisterSignalHandler();
	char* pszAppPath = getAppPath(); //extract application path
	ret = OpcServerMain(pszAppPath);
	if (pszAppPath) delete[] pszAppPath;

	getchar();
	return ret;
}