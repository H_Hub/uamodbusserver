#ifndef _NMBUILDING_H_
#define _NMBUILDING_H_

#include "mongolib.h" 
#include <nodemanagerbase.h>
#include <opcua_dataitemtype.h>
#include "ModbusTcp.h"
#include <memory>

#include "ModbusTcpCon.h"
#include "MongoData.h"

class ModbusReadCoil;
class UaMethodGeneric;

class NmBuilding : public NodeManagerBase
{
	UA_DISABLE_COPY(NmBuilding);
public:
	NmBuilding();
	virtual ~NmBuilding();
	virtual UaStatus afterStartUp() override;
	virtual  UaStatus beforeShutDown() override;
	
	//IOManagerUaNode implementation, update Data
	virtual UaStatus readValues(/*const UaVariableArray& arrUaVariables, UaDataValueArray& arrUaDataValues*/);

	virtual OpcUa_Boolean beforeSetAttributeValue(Session* pSession, UaNode* pNode, OpcUa_Int32 attributeId, const UaDataValue& dataValue, OpcUa_Boolean& checkWriteMask) override;
	//virtual void afterGetAttributeValue(Session * pSession, UaNode* pNode, OpcUa_Int32 attributeId,  UaDataValue& dataValue) override;

	UaVariable* getInstanceDeclarationVariable(UaString stringIdentifier);	
	
	MongoDB* pMongoDB = new MongoDB();

	ModbusTcpCon client;
	ModbusSlave *pSlave = new  ModbusSlave(client.service);
	std::shared_ptr<ModbusSlave> slave = static_cast<std::shared_ptr <ModbusSlave>>(pSlave);
	MongoDataBase mongoDB;
	
	//boost::asio::ip::tcp::endpoint ep502(boost::asio::ip::address_v4::from_string("127.0.0.1"), 502);
	//boost::asio::io_service service;	
	//std::unique_ptr<ModbusTcp> pModbusTcp{new ModbusTcp(service)};
	//Coil* pCoil = new Coil(pModbusTcp.get());
	//Register* pRegister = new Register(pModbusTcp.get());
	OpcUa_Int32 num;

	//SUPPORT FOE METHOD**************
	//MethodManager* getMethodManager(UaMethod* pMethod) const;
	//virtual UaStatus beginCall(MethodManagerCallback* pCallback, const ServiceContext& serviceContext, OpcUa_UInt32 callbackHandle, MethodHandle* pMethodHandle, const UaVariantArray& inputArguments);
	UaFolder* pFolder = NULL;
	vector<int32_t> coil; //store value query from mongodb
	vector<int32_t> reg;
	vector<int32_t> inputCoil;
	vector<int32_t> inputReg;
private:
	UaStatus createTypeNode();
	UaStatusCode getModbusConfig(OpcUa_UInt32 index, UaString& sName, OpcUa_UInt32& address);	
	//Method helpers
	OpcUa::BaseMethod* pMethod = NULL;
	UaPropertyMethodArgument* pPropertyArg = NULL;
	UaUInt32Array nullarray;
	UaVariableArray  arrUaVariables;
	UaDataValueArray  arrUaDataValues;
	UaMethodGeneric* m_pMethod;
	

	
};
#endif