#pragma once
#include "mongolib.h"
#include <algorithm>
#include <map>


class MongoDataBase
{
public:
	MongoDataBase();
	virtual ~MongoDataBase() { m_pMongoDB = nullptr; };
	void getDataMongo();
	std::map<int, int> mListCoil;
	std::map<int, int> mListReg;
	std::map<int, int> mListInputCoil;	
	std::map<int, int> mListInputReg;
private:
	std::vector<int32_t> mCoilVector;
	std::vector<int32_t> mRegVector;
	std::vector<int32_t> mInputCoilVector;
	std::vector<int32_t> mInputRegVector;
	MongoDB* m_pMongoDB;
	void updateMap(std::vector<int32_t>& vector, std::map<int, int>& map);
};