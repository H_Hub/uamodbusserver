#pragma once

#ifdef MONGODBDLL_EXPORTS
#define MONGODBDLL_API __declspec(dllexport)

#else
#define MONGODBDLL_API __declspec(dllimport)
#endif

#include <iostream>
#include <vector>

class MONGODBDLL_API MongoDB_pImpl;

class MONGODBDLL_API MongoDB
{
public:
	MongoDB();
	virtual ~MongoDB();
	void initDB();
	bool connectDB();
	void addRegColDB(int32_t i);
	void addColDB(int32_t i);	
	void delRegColDB(int32_t i);
	void delColDB(int32_t i);
	void pushCoil(std::vector<int32_t>&coil);
	void pushReg(std::vector<int32_t>&reg);
	void addInputCoil(int32_t i);
	void addInputReg(int32_t i);
	void delInputCoil(int32_t i);
	void delInputReg(int32_t i);
	void pushInputCoil(std::vector<int32_t>& inputCoil);
	void pushInputReg(std::vector<int32_t > & inputReg);

private:
	std::unique_ptr<MongoDB_pImpl> m_pImpl;
};
