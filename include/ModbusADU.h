#pragma once
#ifdef MODBUSCLIENT_EXPORTS
#define MODBUSCLIENT_API __declspec(dllexport)
#else
#define MODBUSCLIENT_API __declspec(dllimport)
#endif

#include "RequestInfo.h"

class MODBUSCLIENT_API ModbusADU
{
public:
	ModbusADU(const RequestInfo& requestInfo);
	virtual ~ModbusADU() {};

	void setRequestFrame();
	void setResponseFrame();

	
	unsigned int getaTransID() { return aTransID; }
	bool getexceptionStatus() { return exceptionStatus; }
	int getexceptionCode() { return exceptionCode; }
	unsigned int getqUnitID() { return qUnitID; }
	unsigned int getqFcode() { return qFcode; }
	unsigned int getqStartAddr() { return qStartAddr; }
	unsigned int getqNumberOfData() { return qNumberOfData; }
	unsigned int getqqLength() { return qLength; }

private:
	void setRequestFrameFC1();
	void SetRequestFrameFC2();
	void setRequestFrameFC3();
	void SetRequestFrameFC4();
	void setRequestFrameFC5();
	void setRequestFrameFC6();
	const unsigned int qProtocolID = 0;
	unsigned int  aUnitID, aFCode, sStartAddr, aNumberOfData, aLength;

	//status Variable
	bool illegaStatus;
	bool wrongStatus;
	bool timeoutStatus;

	
	bool exceptionStatus;
	int exceptionCode;
	unsigned int qUnitID, qFcode, qStartAddr, qNumberOfData, qLength;

public:
	unsigned int  qTransID, aTransID;
	std::unique_ptr<bool[]> mCoilData;
	std::unique_ptr<int[]> mRegData;
	std::unique_ptr<bool[]> mInputCoilData;
	std::unique_ptr<int[]> mInputRegData;
	unsigned int qNumberOfDatabytes, aNumberOfDatabytes;
	std::unique_ptr<uint8_t[]> qFrame, aFrame;
	unsigned int qFrameSize;
	unsigned int aFrameSize;
	const unsigned int exceptionFrameSize = 9;


};