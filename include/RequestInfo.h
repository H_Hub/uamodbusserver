#pragma once
#ifdef MODBUSCLIENT_EXPORTS
#define MODBUSCLIENT_API __declspec(dllexport)
#else
#define MODBUSCLIENT_API __declspec(dllimport)
#endif

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <thread>

/*typedef*/ struct MODBUSCLIENT_API RequestInfo
{
	RequestInfo(int unitId, int functionCode, unsigned int  startAdd, unsigned int  numberOfData):mUnitId(unitId), mFunctionCode(functionCode), mStartAdd(startAdd), mNumberOfData(numberOfData)
	{}

	int mUnitId;
	int mFunctionCode;
	unsigned int mStartAdd;
	unsigned int mNumberOfData;
};
