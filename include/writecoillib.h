#pragma once
#ifdef WRITECOILDLL_EXPORTS
#define WRITECOILDLL_API __declspec(dllexport)
#else 
#define WRITECOILDLL_API __declspec(dllimport)
#endif

//Write register, not coil@@
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <cstdint>
#include <thread>
#include <string>
#include <stdio.h>
#include <cstdio>
#include <iostream>
#include <vector>


class WRITECOILDLL_API WriteCoil
{
public:
	WriteCoil();
	virtual  ~WriteCoil();
	void connect(boost::asio::ip::tcp::endpoint & ep);
	void connectHandler(const boost::system::error_code & ec);
	void writeFrame();
	void writeFrameHanlder(const boost::system::error_code& e, size_t byte);
	void readCoil();
	void readCoilHandler(const boost::system::error_code& ec, size_t byte);
	std::vector<int> repVector;
	int addr, num_val;
private:
	std::thread threadWork;
	boost::asio::io_service service;
	boost::asio::io_service::work work;
	boost::asio::ip::tcp::socket sock;
	uint8_t response[1024];
	uint8_t frameSender[12];

};