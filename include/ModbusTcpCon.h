#pragma once
#ifdef MODBUSCLIENT_EXPORTS
#define MODBUSCLIENT_API __declspec(dllexport)
#else
#define MODBUSCLIENT_API __declspec(dllimport)
#endif

#include "ModbusSlave.h"
class MODBUSCLIENT_API ModbusTcpCon
{
public:
	ModbusTcpCon();
	~ModbusTcpCon() { service.stop();  modbusClose(); };
	void modbusRun();
	void modbusClose();
	boost::asio::io_service service;
private:
	std::unique_ptr<boost::asio::io_service::work> mWork;
	std::unique_ptr < std::thread> mThread;
	std::shared_ptr<ModbusSlave> mSlave;
};